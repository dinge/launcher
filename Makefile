simple_start:
	@make -j3 run 

run: yserv cserv fserv
yserv: prog/sync
	python3 prog/sync/main.py
cserv: prog/scale
	python3 prog/scale/main.py
fserv: prog/front
	php -t prog/front/ -S 0.0.0.0:8000


install: prog/sync prog/scale prog/front
update: clean install

prog/sync:
	git clone https://gitlab.com/dinge/sync_backend prog/sync
prog/scale:
	git clone https://gitlab.com/dinge/scale_backend prog/scale
prog/front:
	git clone https://gitlab.com/dinge/frontend prog/front

clean:
	rm -rf prog/
